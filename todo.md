# TODO #


* Hex pockets,
* Hex array of holes circular
* circular array of circular holes
* Rectangular array of holes.
* Circular and rectangualr array of ArchPads
* Arch sweep object.
* Comment object.
* Pause with message verb for cncMill
* Mill of surface while allowing to skip some polygons.
  Look up aluminum milling feed rate,  depth of cut,  spin rates.
  bypass circuit breaker in air compressor
* allen wrench holder to practice on aluminum with.
* Need ability to recognize different bits and automatically adjust tool flute length and bit diam as the bits are selected.
  design for motor compartment lid
  design for motor wheels
  design for main pump compartment
  design for main boiler area.
  make sure fluid flow from main boiler back to pump area.
  parabola square alignment hole should not be using finish speed.
  mill line which goes down one way and comesback deeper the other
  When milling rectangle if the rectangle is less than 1.5 bit diam then use alternative milling strategy which just goes down and back.

* Make transition to adding a set of objects that can be queried and then generate  from loop rather than direct generation currently being used.

* TODO Items for Ruby based CNC Generation Code
* when drawing a radius / arc I should have a calculation that allows the radius calculated points to be stretched or shortened to fit a specified height.

* Restructure code for proper module usage
* Restructure checks for boundary checks to use code blocks rather than cut and paste
* Update circle mill to support layered milling
* Finish conversion and preliminary test of mill square.
* Finish conversion and preliminary test of parabola
* Test algorithm for rotating and element around a circular point.
* Obtain algorithm for calculating points on a curve.

When I create a new shape it needs to know what the surface level around it is so that it can deduct any previously removed material from the calculations when determining flute length

* mill polygon with surrounding square.
   get min_x, max_x from from polygon
   mill polygon rotated
   mill polygon rotated with new x,y

* Write and test code to mill the wing using a polygon array that allows arcs
* Write and test code to mill polygon
* Write and test code to mill polygon exclusive of other defined polygons.

* Update all the shape files to handle proper climb semantics required for machining.

* Code to read and duplicate a Gcode file.
* Code to read and rescale a Gcode file.
* Code to read and rotate X,Y a Gcode file.

* Documentation that can be posted for the Gcode generation.

* Reserach Step and IGS file formats to see how hard it would be to extract the primitives.

* Need to be able to set Z for a object which states that it is known that the pre milled area around the
  object is Z depth prior to starting to mill it.  This is required to allow the bit to only retract that amount
  when milling details.


* Possible user interface that would help visualize.
  In window on the right show the code.     In window on the left show a graphical depiction of  vector moves where the vectors are adjusted to scale based on bit size.     Auto detect  different milling layers depths in 10 discrete layers and asign the milling to the closest layer and show each layer in a different color.    Also provide an option for over under windows on the left where one is showing the top or side view and the other is showing a iso top left view which can be rotated.    Allow script settings that will change the rate of run so some elements can be slowed down to watch the movement.   When rendering if a given line is already rendered in a deeper color then a more shallow movement over the same area should not disrubt that color but should still be able to see the bit movements.   On the top of the right side there should be a series of buttons that will enable a bunch of wizards that will auto expand to take up top quarter of screen with questions.   The Wizard will insert the code where ever the cursor is moved in text window whenever the insert button is pressed.  The Wizard will remember the last settings automatically and offer an option to save those settings under a name for that wizard and they can be retreived with a simple dropdown list.  The GUI should have an option that will auotmatically re-run every time a user moves off the current line with the default setting to be to run through the last line slowing down for the last couple of lines automatically.  It should also have an option of run to end either with or without the automatic slowdown.

* cncShapeArc.rb -  mill_arc Add logic here that will check the radius  against the limits and make sure that the  maximal point on the radius will fit inside  the limits and adjust radius down if it does not fit.