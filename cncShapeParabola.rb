require 'cncMill'
require 'cncBit'
require 'cncMaterial'
require 'cncShapeBase'
require 'cncShapeRect'
require 'cncShapeCircle'
#  See (C) notice in http://cncutil.org/cnc/CNCUtil/license.txt for license and copyright.


# * * * * * * * * * * * * * 
class CNCShapeParabola
# * * * * * * * * * * * * * 
    include  CNCShapeBase
    extend  CNCShapeBase
    # - - - - - - - - - - - - - - - -      
    def initialize(mill, width, focus_y, depth,x_increment=0.1)
    # - - - - - - - - - - - - - - - -    
      base_init(mill)
      @mill         = mill
      @width      = width
      @focus_y   = focus_y
      @depth      = depth
      @y_off       = 0.3
      half_width  = width / 2
      @min_x      = 0 - half_width
      @max_x     = half_width
      @min_y      = @y_off
      @max_y     = @y_off + calc_y(max_x, focus_y)
      @side_off   = 1.0
      @x_increment = x_increment
    end #meth
    
    
    # - - - - - - - - - - - - - - - -    
    def max_x
    # - - - - - - - - - - - - - - - -
       @max_x
    end #meth

    # - - - - - - - - - - - - - - - -    
    def min_x
    # - - - - - - - - - - - - - - - -
       @min_x
    end #meth

    # - - - - - - - - - - - - - - - -    
    def max_y
    # - - - - - - - - - - - - - - - -
       @max_y
    end #meth

    # - - - - - - - - - - - - - - - -    
    def min_y
    # - - - - - - - - - - - - - - - -
       @min_y
    end #meth


    
    # - - - - - - - - - - - - - - - -    
    def focus_y(focus_point=nil)
    # - - - - - - - - - - - - - - - -
      if (focus_point != nil)
        @focus_y = focus_point
        @max_y   = depth_off + calc_y(max_x, focus_point)
      end
      @focus_y
    end #meth
    

    

    
    
    # - - - - - - - - - - - - - - - -    
    def width(aWidth=nil)
    # - - - - - - - - - - - - - - - -
      if (aWidth != nil)
        @width = aWidth
        half_width = aWidth / 2
        @max_x = half_width
        @min_x  = 0 - half_width
        @max_y   = depth_off + calc_y(@max_x, @focus_point)
      end
      @width
    end #meth
    

    # - - - - - - - - - - - - - - - -    
    def calc_y(x_in, fp_in=@focus_y)
    # - - - - - - - - - - - - - - - -
      lcy  = (x_in * x_in) / (fp_in * 4.0)
      lcy
    end #meth


    # mill the arc in layers accordiing to the
    # maximum flute length
    # - - - - - - - - - - - - - - - -      
    def mill_arc(aDepth = @depth)
    # - - - - - - - - - - - - - - - -    
      @mill.retract() 
      if aDepth.abs <= cut_depth_rough.abs
        mill_arc_s(aDepth)
        return
      else
        dd = 0
        dd = 0 - cut_depth_rough.abs      
        while true
          mill_arc_s(dd)
          if (dd <= aDepth)
            break
          end #if
          dd -= cut_depth_rough.abs
          if (dd < aDepth)
           dd = aDepth
          end #if
        end #while
      end #else
    end #meth
       
    # - - - - - - - - - - - - - - - -      
    def mill_arc_s(aDepth)
    # - - - - - - - - - - - - - - - -    
      x =  min_x
      yo = 0
      arc_cut_width =  cut_increment_finish * 0.9  
      start_depth = aDepth   
      cmy = min_y - arc_cut_width
      first_y = cmy + calc_y(x, @focus_y)
      @mill.move_fast(x, first_y, cz)
      @mill.plung(aDepth)
      # Mill Off Material around the Prabola
      set_speed_rough()
      x = @min_x
      # Go one way as rough
      while true
        while x <= @max_x
          y = calc_y(x, @focus_y)
          xo = x
          yo = cmy + y
          @mill.move(xo,yo)      
          x = x + @x_increment
        end #while

        set_speed_finish()
        cmy +=  cut_increment_finish
        if (cmy > min_y)
          cmy = min_y
        end #if


       # Come Back as finished
        x = max_x
        while x >= @min_x
          y = calc_y(x, @focus_y)
          xo = x
          yo = cmy + y
          @mill.move(xo, yo)
          x = x - @x_increment
        end #while

        if (cmy >= min_y)
           break
        end #if

         cmy += cut_increment_finish
         if (cmy > min_y)
             cmy = min_y
         end #if

      end # while 
    end #meth

       
###    # - - - - - - - - - - - - - - - -      
###    def mill_arc_s2(aDepth)
###    # - - - - - - - - - - - - - - - -    
###      x =  max_x
###      yo = 0
###      start_depth = aDepth   
###      @mill.move_fast(@min_x, @max_y + @y_off, cz)
###      @mill.plung(aDepth)
###      # Mill Off Material around the Prabola
###      set_speed_rough()
###      cc = 4
###      while cc >= 0
###        x = @min_x
###        while x <= @max_x
###          y = calc_y(x, @focus_y)
###          xo = x
###          yo = y + @y_off - (bit_diam * (cc+1)) - bit_radius
###          @mill.move(xo,yo)      
###          x = x + @x_increment
###        end #while
###        cc = cc - 1  
###        if (cc == 0)
###          set_speed_finish()
###        end #if
###        x = max_x
###        while x >= @min_x
###          y = calc_y(x, @focus_y)
###          xo = x
###          yo = y + @y_off - (bit_diam * (cc+1)) - bit_radius
###          @mill.move(xo, yo)
###          x = x - @x_increment
###        end #while
###        cc = cc - 1  
###      end # while 
###    end #meth
###


    # - - - - - - - - - - - - - - - -      
    def do_mill    
    # - - - - - - - - - - - - - - - -        
      # # # # # # # #
      # # # BEGIN MAIN
      # # # # # # # # 
      aRect =  CNCShapeRect.new(@mill) 
      three_eights = (3.0 / 8.0) * 0.95
      set_speed_rough()
      # draw vertical line up the center
      @mill.move(0, 0)
      @mill.plung(-0.1)
      @mill.move(0, @max_y + 3)

      # Draw Horizontal line to allow final 
      # trim
      set_speed_rough()
      @mill.retract() 
      @mill.move_fast(@min_x, @max_y + 3)
      @mill.plung(-0.1)
      @mill.move(@max_x, @max_y + 3)
      @mill.retract()

      align_y = min_y + 2.5
      mid_y = (max_y + min_y) / 2
      if (mid_y) > (align_y + 2)
         align_y = mid_y
      end #if

      #print "(min_y=", min_y, " max_y = ", max_y, " align_y=",  align_y, "  mid_y = ",  mid_y, ")\n"


       @mill.retract()
      # Mill out center circle for the aiming axel
      aCircle = CNCShapeCircle.new(@mill)
      aCircle.mill_pocket(@max_x - 1.0, align_y, three_eights,  @depth)
      @mill.retract()
      set_speed_rough()

      # mill out rectangular alignment hole
      aRect.centered(0, @mill.max_y - 0.5, 0.5, 0.5, @depth)
      aRect.do_mill()  # Mill out alignment hole
      @mill.retract()
      

      # Mill out center circle for the aiming axel
      @mill.retract()
      aCircle = CNCShapeCircle.new(@mill)
      aCircle.mill_pocket(0, min_y + 1.5, 1.04, @depth)
      @mill.retract()
      set_speed_rough()


      # mill out rectangular alignment hole
      #aRect.centered(@min_x+1.0, align_y, 0.5, 0.5, @depth)
      #aRect.do_mill()  
      #@mill.retract()


      # Mill out center circle for the aiming axel
      aCircle = CNCShapeCircle.new(@mill)
      aCircle.mill_pocket(@min_x + 1.0, align_y,  three_eights,  @depth)
      @mill.retract()
      set_speed_rough()



      # Mill out the actual Arc
      mill_arc(@depth)  
      @mill.retract()
      set_speed_rough()

      # mill out flat edge on  side of parabola
      # will auto truncate  at machine limit for y
      # which is about 4 inches on mini mill.
      @mill.retract()
      aRect.skip_finish_cut()
      aRect.reset(
          @min_x - 0.1,
          0.1, 
          @min_x , 
          @max_y + @focus_y, 
          @depth)
      aRect.skip_finish_cut()
      aRect.do_mill()
      @mill.retract()
      set_speed_rough()

      # mill out flat edge on other side of
      # parabola         
      # will auto truncate  at machine limit for y
      # which is about 4 inches on mini mill.
      aRect.reset(max_x, 
            0.1,
            @max_x + 0.1, 
            @max_y + @focus_y, 
            @depth)
      aRect.skip_finish_cut()
      aRect.do_mill()
      @mill.retract()



      # Move table to home position for next
      # milling operation
      @mill.home()

   
    end #meth
  end #class
