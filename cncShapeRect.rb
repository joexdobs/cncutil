require 'cncMill'
require 'cncBit'
require 'cncMaterial'
require 'cncShapeBase'
#  See (C) notice in http://cncutil.org/cnc/CNCUtil/license.txt for license and copyright.

  # *****************************************
  class CNCShapeRect 
  # *****************************************
    include  CNCShapeBase
    extend  CNCShapeBase

     # initialize a rectrangle for milling
     # requires a center point, width
     # and length.
     # - - - - - - - - - - - - - - - -   
     def initialize(mill,  x=0, y=0, width=0.5, len=0.5, depth=-0.5)
     # - - - - - - - - - - - - - - - -
         base_init(mill,x,y,mill.cz,depth)
         #print "rect initialize after base_init depth=", @depth, "\n"
         len = len.abs
         width = width.abs
         @len     = len
         @width = width
         @lx       = x
         @ly       = y
         @mx     = x + width
         @my     = y + len
         @include_finish_cut = true
         self
       end  # init 


      # Force the square to be centered over
      # the X,Y coordinate rather than using
      # X,Y as first corner.
      # - - - - - - - - - - - - - - - -     
      def centered(scx=@x,scy=@y,width=@width,length=@len, depth=@depth)
      # - - - - - - - - - - - - - - - -  
        #print "(centered scx=", scx, " scy=", scy, " width=", width, " length = ", length,  " depth= ", depth, ")\n"
        @width = width.abs
        @len     = length.abs
        @depth =depth
        wr = width / 2
        lr  = length / 2
        @lx       = scx - wr  
        @ly       = scy - lr
        @mx     = scx + wr
        @my     = scy + lr
        self
      end #meth

      # Force the square to be centered over
      # the X,Y coordinate rather than using
      # X,Y as first corner.
      # - - - - - - - - - - - - - - - -     
      def reset (lx,  ly,  mx,  my,  depth=@depth)
      # - - - - - - - - - - - - - - - -     
        if (lx > mx)
          tx = lx
          lx  = mx
          mx = tx
        end #if

        if (ly > my)
          ty = ly
          ly  = my
          my = ty
        end

        @width = mx - lx
        @len    =  my - ly
        @depth =depth
        @lx   = lx
        @ly   = ly
        @mx = mx 
        @my = my 
        self
      end #meth

     #- - - - - - - - - - - - - - - - - - 
     def include_finish_cut
     #- - - - - - - - - - - - - - - - - -
        @include_finish_cut = true
     end # meth

     #- - - - - - - - - - - - - - - - - - 
     def skip_finish_cut
     #- - - - - - - - - - - - - - - - - -
        @include_finish_cut = false
     end # meth


     # Perform the actual milling to based on current parameters
     #- - - - - - - - - - - - - - - - - -
      def do_mill
     #- - - - - - - - - - - - - - - - - -
       mill_cavity(@lx,@ly,@mx,@my, @depth)
       self
      end #meth


     # - - - - - - - - - - - - - - - -     
     def mill_cavity(lx,ly, mx, my, depth)
     # - - - - - - - - - - - - - - - -     
       print " (mill cavity @depth=", @depth, " lx=", lx, " ly=", ly, " mx=", mx, " my=",my, " depth=", depth,  ")\n"
      
     # swap the X values to make mx the high one
      if (lx > mx)
        print "(mill_cavity swap lx,mx)\n"
        tx = mx
        mx = lx
        lx = tx
      end #if
       
     # swap Y values to make my the high one
      if (ly > my)
        print "(mill_cavity swap ly,my)\n"
        ty = my
        my = ly
        ly = ty
      end #if

      # Adjust the interior Cavity
      # to allow for the size of the
      # bit or the milled cavity would 
      # be too large   
      lx += bit_radius
      ly += bit_radius
      my -= bit_radius
      mx -= bit_radius  
      #print "(after adjust for bit radius  lx=",lx, " ly=",ly, " mx=",mx, " my=", my, " bit_radius=", bit_radius, ")", "\n"


      # if the bit is not already over the cavity to be
      # milled then we have to retract it before moving
      # to that location
      if ((@mill.cx < lx) or (@mill.cx > mx) or (@mill.cy < ly) or (@mill.cy > my)) 
        print "(mill_rect auto rectract)\n"
        @mill.retract()
        @mill.move_fast(lx,ly, @mill.cz)
      end #if

       if depth.abs <= cut_depth_rough.abs
         #print "(simple mill square depth", depth, ")", "\n"
         mill_cavity_s(lx,ly,mx,my,depth)
         return
      else
        dd = 0
        dd = 0 - cut_depth_rough.abs      
        while true
          mill_cavity_s(lx,ly,mx,my,dd)
          if (dd <= depth)
            break
          end #if
          dd -= cut_depth_rough.abs
          if (dd < depth)
            dd = depth
          end #if
        end #while
      end #else
      self
    end #meth


    # - - - - - - - - - - - - - - - -     
    def mill_cavity_s(lx,ly, mx, my, dd2)
    # - - - - - - - - - - - - - - - -     
      #print "(mill rect cavity lx=",lx, " ly=",ly, " mx=",mx, " my=", my,  " depth=", dd2, ")", "\n"

      @mill.plung(dd2)  
      lcx = lx
      lcy = ly
     

      # start at inside and mill
      # towards the outside
      dx = (mx - lx).abs
      dy = (my - ly).abs
      mid_x = lx + (dx / 2)
      mid_y = ly + (dy / 2)
      no_pass_x = ((dx / cut_increment_rough) / 2)
      no_pass_y = ((dy / cut_increment_rough) / 2)
      #print "(cut_increment_rough=", cut_increment_rough, ")\n"
      # if we have a long narrow cut we want to emphasize
      # cutting along the long axis to save time.
      if dy > dx
        # difference in Y is greater than difference in X so 
        # emphasize cutting on Y first.
        percent_dif = dx / dy
        begylen = dy - (no_pass_x  * cut_increment_rough)
        if (begylen < bit_diam)
          begylen = bit_diam
        end
        begxlen = bit_radius
        no_pass = no_pass_x
        #print "(mill_rect emphasize Y begylen=", begylen, "  begxlen=", begxlen, ")\n"
        #print "( dx=", dx, "  dy=", dy,  " mid_x=", mid_x,  " mid_y=", mid_y, ")\n"
      else
        # emphasize cutting on the X axis first
        percent_dif = dy / dx
        begxlen = dx - (no_pass_y * cut_increment_rough)
        if (begxlen < bit_diam)
          begxlen = bit_diam
        end
        begylen = bit_radius
        no_pass = no_pass_y
        #print "(mill_rect emphasize X  begxlen=", begxlen, " begylen=", begylen, ")\n"
        #print "( dx=", dx, "  dy=", dy,  " mid_x=", mid_x,  " mid_y=", mid_y, ")\n"
      end #i

      
      #print "(begxlen = ", begxlen, "  begylen=", begylen,  " mid_x=", mid_x, "  mid_y =", mid_y, "  no_pass=", no_pass, ")\n"


      # Based on the chosen favorable cut axis 
      # calcualte the initial length of each X,Y
      clx   = mid_x -  (begxlen / 2)
      cly   = mid_y - (begylen / 2)
      cmx = mid_x + (begxlen / 2)
      cmy = mid_y  + (begylen / 2)

      if (clx < lx)
        clx = lx
      end
      if (cly < ly)
         cly = ly
      end
      if (cmx > mx)
        cmx = mx
      end
      if (cmy > my)
        cmy = my
      end


      #print "(L217:  clx=", clx, "  cly=",  cly,  " cmx=", cmx, "  cmy=", cmy, ")\n"
 
 

      if ((mx - cmx) < cut_increment_rough && ((my - cmy) < cut_increment_rough))  || (@include_finish_cut == false)
        curr_cut_inc  = cut_increment_rough
        set_speed_rough
      else
        curr_cut_inc  = cut_increment_finish
        set_speed_finish
      end

      while true
        #print " (L228  clx=", clx, "  cly=",  cly,  " cmx=", cmx, "  cmy=", cmy, ")\n"
        @mill.move(clx,cly)
        @mill.move(clx,cmy)
        @mill.move(cmx,cmy)
        @mill.move(cmx,cly)
        @mill.move(clx,cly)

        if (cmx == mx) && (cmy == my)
          #print "(exactly matched requeseted raidius so done)\n"
          break  # finished the loop
        end  #if 

        if (((cmx + curr_cut_inc) > mx) || ((cmy + curr_cut_inc) > my)) &&  (@include_finish_cut == true)
          #print "(L240 starting finish cut)"
          set_speed_finish
          curr_cut_inc = cut_increment_finish
        end

        clx -= curr_cut_inc
        cly -= curr_cut_inc
        cmx += curr_cut_inc
        cmy += curr_cut_inc

        if (cmx > mx)
          #print "(exceed mx ", mx, " so set back)\n"
          cmx = mx
        end

        if (clx < lx)
          #print "(exceed lx=", lx, " clx=", clx, ")\n"
          clx = lx
        end

        if (cmy > my)
          #print "(exceed my", my, " so set back)\n"
          cmy = my
        end
       
       
       if (cly < ly)
          #print "(exceed ly=", ly, "  cly=", cly, ")\n"
          cly  =  ly
       end
      end #while
      #@mill.move(lx,ly) # setup for next pass
      set_speed_rough
    end #meth       
  end # class
