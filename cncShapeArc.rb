# CNCChapeArc
#  See (C) notice in http://cncutil.org/cnc/CNCUtil/license.txt for license and copyright.
require 'cncMill'
require 'cncBit'
require 'cncMaterial'
require 'cncShapeBase'
require 'cncGeometry'





   # PRIMITIVE: Mill a arc from the current location to the 
   #  destination location.    Normally will only support
   #  1/4 of a circle or less.   Caller must pre-position
   #  bit and set depth
   # does not properly handle multiple passes
   # for deep cuts.  If multiple passes are
   # needed use the CNCShapeObject.
   # - - - - - - - - - - - - - - - - - -
   def mill_arc(mill, xo,yo,radius, speed=nil,  direction = "G03")
   # - - - - - - - - - - - - - - - - - -
     if (speed == nil)
      speed = mill.speed
     elsif (speed == "G03") || (speed == "G02")
       # allows us to keep old functions
       # that where not passing speed working
       direction = speed
       speed = mill.speed
     end #if


     cp = mill.apply_limits(xo,yo)
       # Adjust our X,Y coordinates to fit
       # inside our machines currently defined
       # limits.

     opcode = "G03"
     if (direction == "G02") || (direction == "clockwise")
       opcode = "G02"
     end

   

     print opcode, " X", sprintf("%8.3f", cp.x), " Y",sprintf("%8.3f", cp.y) ,  " R",sprintf("%8.3f", radius) ,  " F",  speed, "\n"
    @cy = yo
    @cx = xo
   end #meth





# PRIMITIVE: Mill a arc segment from beg_angle to 
# ending angle where the arc radius
# either increases for decreases as it
# progresses.  Calculates arc points on degree_inc
# and will mill to specified depth
# Does not handle multiple passes needed
# to reach depth.  If multiple passes
# are needed use the CNCShapeArc object
# instead.
# - - - - - - - - - - - - - - - - - - - -
def  changing_arc(mill, circ_x,circ_y, beg_radius, beg_angle, end_radius, end_angle, 
depth = nil,  degree_inc=nil)
# - - - - - - - - - - - - - - - - - - - -
  #print "(changing_arc  cir_x=", circ_x,  " circ_y=", circ_y, " beg_radius=", beg_radius, " beg_angle=", beg_angle, "  end_radius=", end_radius, " end_angle=", end_angle, " depth=", depth,  "degree_inc =", degree_inc, ")\n"
  if (depth == nil)
    depth = mill.cz
  end #if

  if (degree_inc == nil)
    degree_inc = 0.5
  end #if

  curr_deg    = beg_angle
  degree_inc = degree_inc.abs
  radius_change = (end_radius - beg_radius)
     # radius_change may be negative
     # in which case it is spiraling in
     # otherwise it is spiraling out
  sweep = (end_angle - beg_angle).abs
  no_steps = sweep / degree_inc
  radius_change_per_deg_inc  = radius_change / no_steps
  curr_radius= beg_radius
  
    pt_begin = calc_point_from_angle(circ_x,circ_y, curr_deg, curr_radius)


  # if the current CX is not verry close to the
  # starting of this arc then we need to retract
  # prior to milling. 
  if ((mill.cx - pt_begin.x).abs > mill.bit_diam) ||
  ((mill.cy - pt_begin.y).abs > mill.bit_diam)
    #print "(changing arc auto retract)\n"
    mill.retract()
  end #else
  mill.move(pt_begin.x, pt_begin.y)
  mill.plung(depth)

  while (true)
      #print "(changing_arc  curr_deg=", curr_deg, "  curr_radius=", curr_radius, " circ_x = ", circ_x,  "  circ_y=", circ_y, ")\n"
      cp = calc_point_from_angle(circ_x,circ_y, curr_deg, curr_radius)
      #print "(changing_arc cp.x=", cp.x, " cp.y=", cp.y, ")\n"
      mill.move(cp.x, cp.y)
      if (curr_deg == end_angle)
        break    # finished milling final
                     # part of segment so done
      end #if

      # update current degree. This additional
      # logic allows this method to mill both
      # an increasing arc and a decreasing arc
      if beg_angle < end_angle
         # billin forward from lower degrees
         # towards higher ones
         curr_deg += degree_inc
         if curr_deg > end_angle
            curr_deg = end_angle
         end #if
      else # going in reverse
         curr_deg -= degree_inc
         if curr_deg < end_angle
           curr_deg = end_angle
         end
      end #else


      curr_radius += radius_change_per_deg_inc
         # radius change_per_deg_inc may be negative
         # if end_radius is less than beg_radius and if
         # so will spiral in.  Otherwise will spiral out.
     
   end #while
end   # Meth


# - - - - - - - - - - - - - - - - - - - -
def calculate_extent(beg_angle, beg_radius, end_angle, end_radius)
# - - - - - - - - - - - - - - - - - - - -
  # have to calculate points to allow
  # us to make decisions containment
  mid_deg    = (beg_angle  + end_angle ) / 2
  mid_radius= (beg_radius + end_radius) / 2
  pt_mid1 =  calc_point_from_angle(circ_x,circ_y, mid_deg, beg_radius)
  pt_mid2 =  calc_point_from_angle(circ_x,circ_y, mid_deg, beg_radius)
  pt_end  = calc_point_from_angle(circ_x,circ_y, end_angle, end_degree)
  
  max_x = max(pt_begin.x, pt_end.x, pt_mid1.x, pt_mid2.x, pt_end.x)
  min_x = min(pt_begin.x, pt_end.x, pt_mid1.x, pt_mid2.x, pt_end.x)
  max_y = max(pt_begin.y, pt_end.y, pt_mid1.y, pt_mid2.y, pt_end.y)
  min_y = min(pt_begin.y, pt_end.y, pt_mid1.y, pt_mid2.x, pt_end.x)
 
  # NEED NEW DATA STRUCTURE
  # CNC extent that can be returned
  # as part of this function
  return nil
end #meth


# creates an object to create a 
# arc segment milled pocket
# and calls for the mill operation
# returns the object created so
# it can be re-used.
# - - - - - - - - - - - - - - - - - - - -
def  arc_segment_pocket(
   mill, 
   circ_x,
   circ_y,
   min_radius,
   max_radius,  
   beg_angle,
   end_angle,  
   depth = nil,
   degree_inc = nil)
# - - - - - - - - - - - - - - - - - - - -
    print "(arc_segment_pocket circ_x=", circ_x," circ_y=", circ_y, " min_radius=", min_radius, " max_radius=", max_radius, " beg_angle=", beg_angle, " end_angle=", end_angle, " depth=", depth, "degree_inc=", degree_inc,  ")\n"

    aArc = CNCShapeArc.new(
                          mill, circ_x,
                          circ_y,
                          min_radius, 
                          max_radius,  
                          beg_angle, 
                          min_radius, 
                          max_radius, 
                          end_angle,  
                          depth,
                          degree_inc)
    aArc.do_mill()
    return aArc
end #meth



# creates an object to create a 
# arc segment milled pocket
# and calls for the mill operation
# returns the object created so
# it can be re-used.
# - - - - - - - - - - - - - - - - - - - -
def  arc_segment_pocket_adv(
   mill, 
   circ_x,
   circ_y,
   beg_min_radius,
   beg_max_radius,  
   beg_angle,
   end_min_radius, 
   end_max_radius, 
   end_angle,  
   depth = nil,
   degree_inc = nil)
# - - - - - - - - - - - - - - - - - - - -
    # print "(arc_segment_pocket depth=", depth, ")\n"

    aArc = CNCShapeArc.new(
                          mill, circ_x,
                          circ_y,
                          beg_min_radius, 
                          beg_max_radius,  
                          beg_angle, 
                          end_min_radius, 
                          end_max_radius, 
                          end_angle,  
                          depth,
                          degree_inc)
    aArc.do_mill()
    return aArc
end #meth




  # *****************************************
  class CNCShapeArc
  # *****************************************
    include  CNCShapeBase
    extend  CNCShapeBase


 

    # - - - - - - - - - - - - - - - - - - 
    def initialize(mill,  x,  y,  
        beg_min_radius,   beg_max_radius, beg_angle=0,
        end_min_radius=nil, end_max_radius=nil, end_angle=360,
        depth=nil,  degree_inc=nil
        )
    # - - - - - - - - - - - - - - - - - -
      base_init(mill,x,y,nil,depth)
      @beg_min_radius  = beg_min_radius
      @beg_max_radius  = beg_max_radius
      @beg_angle       = beg_angle
      @end_min_radius  = end_min_radius
      @end_max_radius  = end_max_radius
      @end_angle       = end_angle   
      @degree_inc      = degree_inc
      @depth           = depth
      @needs_check_parms = true

      #print "(CNCShapeArc init depth=", depth,  " @depth=", @depth, ")\n"
      #check_parms
      return self
    end #meth

  # check_parms needs to be called 
  # whenever the min_max radius have
  # been changed.  Normally this is
  # only during reset or initialization
  # could not figure out how to get
  # the constructor to call it or would
  # have done automatically.  As it is
  # the constructor sets a flag and
  # any method that may need to have
  # bit_adjusted radius dimensions
  # should call check_parms.
  # - - - - - - - - - - - - - - - - - -
  def  check_parms
  # - - - - - - - - - - - - - - - - - -
    if (@needs_check_parms == false)
      return false # didn't need to run
    else
      @needs_check_parms = false
      # reset flag so we don't keep
      # adjusting the parms which would
      # keep deducting the radius from
      # them.
    end #if

    # Swap if needed to make sure min
    # really is smallest on beginning
    if (@beg_min_radius > @beg_max_radius)
      tt = @beg_min_radius
      @beg_min_radius = @beg_max_radius
      @beg_max_radius = tt
    end #if

    # swap if needed to make sure
    # min really is smallest on 
    # beginning
    if (@end_min_radius > @end_max_radius)
      tt = @end_min_radius
      @end_min_radius = @end_max_radius
      @end_max_radius = tt
    end # if

    
    #print "(check_parms @beg_min_radius=", beg_min_radius, " bit_radius=", bit_radius,")\n"

    # Adjust for Bit radius
    @beg_min_radius   += bit_radius
    @end_min_radius   += bit_radius
    @beg_max_radius  -= bit_radius
    @end_max_radius  -= bit_radius
    return self
  end #meth

    # - - - - - - - - - - - - - - - -     
    def beg_min_radius(oi=nil)
    # - - - - - - - - - - - - - - - -     
      if (oi != nil)
        @beg_min_radius = oi
      end #if
      return @beg_min_radius
    end  # meth
       


    # - - - - - - - - - - - - - - - -     
    def beg_max_radius(oi=nil)
    # - - - - - - - - - - - - - - - -     
      if (oi != nil)
        @beg_max_radius = oi
      end #if
      return @beg_max_radius
    end  # meth
       

    # - - - - - - - - - - - - - - - -     
    def beg_angle(oi=nil)
    # - - - - - - - - - - - - - - - -     
      if (oi != nil)
        @beg_angle = oi
      end #if
      return @beg_angle
    end  # meth
       

    # - - - - - - - - - - - - - - - -     
    def end_min_radius(oi=nil)
    # - - - - - - - - - - - - - - - -     
      if (oi != nil)
        @end_min_radius = oi
      end #if
      return @end_min_radius
    end  # meth
       

    # - - - - - - - - - - - - - - - -     
    def end_max_radius(oi=nil)
    # - - - - - - - - - - - - - - - -     
      if (oi != nil)
        @end_max_radius = oi
      end #if
      return @end_max_radius
    end  # meth
       

    # - - - - - - - - - - - - - - - -     
    def end_angle(oi=nil)
    # - - - - - - - - - - - - - - - -     
      if (oi != nil)
        @end_angle = oi
      end #if
      return @end_angle
    end  # meth
       

    # - - - - - - - - - - - - - - - -     
    def degree_inc(oi=nil)
    # - - - - - - - - - - - - - - - -     
      if (oi != nil)
        @degree_inc = oi
      end #if
      return @degree_inc
    end  # meth


  # does the actual milling operation
  # and properly handels multiple passes
  # when milling deep pockets.
  # - - - - - - - - - - - - - - - - - - - -
  def  do_mill
  # - - - - - - - - - - - - - - - - - - 
    check_parms()
    return do_mill_s
  end #meth


  # does the actual milling operation
  # at a previously set depth
  # - - - - - - - - - - - - - - - - - - - -
  def  do_mill_s
  # - - - - - - - - - - - - - - - - - - - -
    #print "(mill arc segment pocket beg_angle=", beg_angle,  "  end_angle=",  end_angle,   " depth = ", depth, " @depth=", @depth, ")\n"

    bit_radius_half = bit_radius / 2
    beg_mid_radius = (beg_min_radius + beg_max_radius) / 2.0
    end_mid_radius = (end_min_radius + end_max_radius) / 2.0
    cut_inc = cut_increment_rough

    #mill.retract()
    cibr = beg_mid_radius - bit_radius_half
    cier = end_mid_radius - bit_radius_half
    cmbr = beg_mid_radius   + bit_radius_half
    cmer = end_mid_radius + bit_radius_half
    
    while(true)
       if (cibr < beg_min_radius)
         cibr = beg_min_radius
       end #if

       if (cier < end_min_radius)
         cier = end_min_radius
       end # if

       if (cmbr > beg_max_radius)
         cmbr = beg_max_radius
       end

       if (cmer > end_max_radius)
         cmer =  end_max_radius
       end #if

       #cp = calc_point_from_angle(cx,cy, beg_angle, cibr)
       #mill.retract()
       #mill.move(cp.x, cp.y)
       #mill.plung(depth)
   

       changing_arc(mill, x,y,cibr, beg_angle, cier, end_angle, depth, degree_inc)  # mill increasing
       #mill.retract()


       changing_arc(mill, x,y,cmer, end_angle, cmbr, beg_angle, depth, degree_inc)
       mill.retract()  # mill decreasing



       if ((cibr <= beg_min_radius) &&
         (cier <=  end_min_radius) &&
         (cmbr >= beg_max_radius) &&
         (cmer >= end_max_radius))
           break
       end 

       cibr -= cut_inc
       cier -= cut_inc
       cmbr += cut_inc
       cmer += cut_inc
     end  #while

   end #meth







  # First you define an arc segment to mill to describe the
  # first arc segment you desire and then you call this function
  # rather than  do_mill which will mill an array of these objects
  # where each one moves degree_inc further around the circle.
  # If degree_between is equal to 0 then it will be set to 1/2 
  # the sweep angle of the current object.  If the
  # num_elem is 0 then the the system will calculate it a the 
  # number that will fit in a 360 degree circle with the current
  # sweep (end_angle - beg_angle) + a 1/2 sweep angle between
  # each item.
  # - - - - - - - - - - - - - - - - - - - -
  def circ_array(num_elem=nil, degree_between=nil)
  # - - - - - - - - - - - - - - - - - - - -
     curr_beg_angle   = @beg_angle
     curr_end_angle   = @end_angle
     sweep_angle      = @end_angle -  @beg_angle

     if (degree_between == nil)
       degree_between = sweep_angle / 2.0
       degree_between_specified = false
     else
       degree_between = 0.0 + degree_between
       degree_between_specified = true
     end #if
     

     # Determine the number of elements we
     # can have
     if (num_elem == nil) || (num_elem == 0)
       degree_inc = sweep_angle + degree_between
       num_elem  = Integer(360.0 / degree_inc)
     elsif (num_elem * sweep_angle) > 360
       max_ele = Integer(360 /sweep_angle)
       degree_inc = sweep_angle + degree_between
     end #if


     #degree_inc = 360.0   
     #Adjust space between to get even spacing
     #if (degree_between_specified == false)
     #  total_sweep = (sweep_angle * num_elem)
     #  extra_sweep = 360 - total_sweep
     #  degree_between = extra_sweep / (num_elem - 1)
     #  print "(recalced degree_between=", degree_betwee, ")\n"
     #end #if

     if (degree_between_specified == true)
       degree_inc = sweep_angle + degree_between
     else
       degree_inc = 360 / num_elem
     end #if

     # illustrate an easy way to get a
     # repeating array of arc pockets
     for ele_cnt in (1..num_elem) 
        @beg_angle = curr_beg_angle 
        @end_angle = beg_angle + sweep_angle
        do_mill()
        mill.retract()
        curr_beg_angle += degree_inc
        if (curr_beg_angle > 360)
          curr_beg_angle = curr_beg_angle % 360
        end #for
     end #for
  end #meth




end #class


