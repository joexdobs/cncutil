# cncBit.rb
#
# The loaded config files specifiy an array of 
#  off RPM,  load per tooth and recomended maximum
#  cut depth for each material.  These can be used by
#  the local bit to calculate maximum depth and feed
#  rates for a given bit.
#
#  normal consensus is to always feed a shallow depth
#  and move at a faster speed rather than take deep
#  cuts.  this changes however when doing finsh cuts
#  on sidewalls where we want to take the maximum 
#  cut depth allowed by the flute while reducing the
#  amount of cut being taken into the sidewall and
#  reducing speed.
#
# In generally we always equate everything to feed per tooth
# which we can adjust for varying conditions.   We get our
# maximum rpm for a given bit size from the material however
# our local bit can adjust that setting such as carbide which
# is allowed to take a larger bite per tooth and run to run
# at a 100% higher RPM than high speed steel.
#
#
# SFM = surface feet per min = RPM * cutter_diam * 0.262
# Chip Load = Feed Rate (in / min) / (RPM * number of cutting teeth)
#    Chiploads are generally set from 0.002 to 0.005 however
#    aluminum can tolerate 0.15 when taking shallow cuts.
#
# feed per tooth is the bite of material each tooth takes as it
# comes around.  For example if a two flute bit is spinning at 1,000 RPM
# then is is taking 2000 cuts per minut or 2000 / 60 = 33 cuts per second so
# if we want it to take 0.01 inchs per cut we would have to move it at a rate
# of 33 * 0.01 = .33 inches per second or 19.8 inches per minute.
#
# If the cut depth is increased then we have to decrease the feed rate
# by a proportional amount.   This will only matter if we need to finish 
# cut a sidewall so that we don't see all the layered cuts.
#
# this calculation is based on the a full width cut.  when we take
# a fractional cut we can increase the speed proportionally .
#
# ips (inch_per_second)  =  (rpm / 60) * number_of_teeth) * per_tooth_load
#                           ((1000 * 60)* 2) * 0.01) = 0.33
# 
# ips has to be adjusted for current cut depth 
#
#     ips = ips * (recomended_cut_depth / (cur_cut_depth / recomended_cut_depth))
#        so for a cut depth of 1" when the recomended cut
#        depth is 0.10  would solve as
#    ips = old_ips (0.33) * (0.1 / (1.0 / 0.1) = 0.01
#
#   ips also has to be adjusted for the cuting swath so
#   so for a cutting swath that is only 1/4" the full swath
#   ips would be increased by 50%.  Only 50% of the bit cuts
#   anyway to we have to deduct the other 50%
#
#  See (C) notice in http://cncutil.org/cnc/CNCUtil/license.txt for license and copyright.


require 'cncMaterial'

  ##########################################
  class CNCBit
  ##########################################

    # default initializer sets the following instance
    # variables.   Bits are normally loaded with 
    # parameters automatically by reading in and
    # evaluating a config file.  By convention these
    # config files are stored in the config/bit 
    # sub directory.
    # * @diam
    # * @length
    # * @flute_len
    # * @cut_depth
    #
    #  The config file name is read and evaluated
    #  to insert values specific for this bit.
    #  such as hardness, lenght, diameter, etc.
    #  The config file is composed of a series
    #  of single line ruby statements that subject
    #  to some limitation can call any setter / getter
    #  for valid for this object.
    #
    #  Bits are normally loaded automatically by
    #  the main machine config process however they
    #  can be loaded into bit # slots of 1 to 30
    #  with explicit calls.
    #
    #
    #  TODO:  Actually read and process the config
    #   file
    def initialize(config_file_name = nil)
      @diam        = 0.11
      @length      = 0.5
      @flute_len   = 1.2
      @cut_depth = @flute_len
      if (config_file_name != nil)
        #TODO: Add the code to read
        # the acutal config file and
        # interpolate it into local 
        # variables.
      end # if

      @aluminum_rpm = 3500
      if (@diam < (3/16))
        @aluminum_max_face_cut = @diam / 8
      else 
        @aluminum_max_face_cut = @diam / 4
      end
    end # init 


    #  Returns Total length of the cutting area
    #  of the bit. This is the maximum
    #  amount the bit could ever cut 
    #  in a single swath.  The actual
    #  amount cut in a single swath is
    #  obtained from cut_depth_inc_curr
    #  which adds in calculations such as
    #  bit and material composition.
    #
    # * Maximum Cutting Area of bit
    #
    def flute_len
      return @flute_len
    end #meth


    def flute_len=(aLen)
      @flute_len = aLen  
    end #meth


    #  total length of the bit.  This is used
    #  to determine how deep this bit can
    #  plung total before the collet or holder
    #  would impact with the work surface.
    #
    #  * Total length of bit protruding from Collet which
    #    has a diameter equal to or less than flute cutting
    #    diameter.
    #
    def bit_len
      return @length
    end #meth


    #
    def bit_len=(aNum)
      @length = aNum
    end #meth


    # Returns Diameter of the cutting diameter for this
    # bit.  If the bit was plunged into the material 
    # and made a single cut line.  That line would be
    # exactly diam (this number) wide.
    def diam
       @diam
    end #meth
    
    #
    def diam=(aDiameter)
       @diam = aDiameter
    end #meth


    # Returns a number which is 1/2 the cutting diameter
    # of this bit.
    def radius
       @diam / 2.0  
    end #meth
     
    

    # The amount the bit can be safely 
    # advanced into the material for
    # a single side cut when making rough
    # cuts especially for pocketing.  This
    # number is combined with the 
    # cut_depth_increment and is derived
    # predominantly from a combination of
    # flute length,  bit composition
    # and material hardness.
    #
    # TODO: enhance this so that it
    # is using bit and material
    #
    def cut_increment_rough
      @diam * 0.85
    end #meth

    #  Same as cut_increment_rough
    #  but generally a smaller number
    #  because it is easier to get high
    #  quality finish cuts when removing
    #  less material.
    #  TODO: enhance this so that it
    #   is using bit and material
    #
    def cut_increment_finish
      @diam * 0.35
    end #meth 

   
    #  The maximum depth of a safe cut
    #  when milling a full bit diameter
    #  swath through the work material
    #  when rough cutting.
    #
    #  This is generally faster for
    #  rough cuts and lower for finish
    #  cuts and is faster for soft 
    #  materials like Strofoam than it
    #  is for harder materials.
    # 
    #  TODO: enhance this so that it
    #   is using bit and material
    #
    def cut_depth_rough
      @flute_len * 0.9
    end #meth 

    #  Same as cut_depth_rough but is
    #  generally a smaller number because
    #  it is easier to get a high quality
    #  finish when removing less material.
    #
    #  TODO: enhance this so that it
    #   is using bit and material
    # - - - - - - - - - - - - - - - -
    def cut_depth_finish
      @flute_len * 0.2
    end #meth 

   #  Returns the current cutting 
   #  depth increment.  This is based
   #  on the current material, type 
   #  of bit,  size of mill and 
   #  whether finish cutting or rough
   #  cutting.   The mill will make this
   #  number have to make a number of
   #  passes to make deeper cuts and the
   #  total number is generally calculated
   #  by dividing total depth of cut by
   #  this number.   The method 
   #  set_speed_finish will generally cause
   #  this method to returrn a smaller number
   #  than when set_speed_rough is used.
   #
   #  * TODO: enhance this so that it
   #  * is using bit and material
   #  * knowledge to determine proper
   #  * cut depth.
   #
   def cut_depth_inc_curr
     return cut_increment_rough
   end #meth 


  end #class 


